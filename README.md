# Les Transmusicales

L’objectif de ce Projet OpenData est de développer par groupe de 2 étudiants une application mobile en Flutter permettant d’exploiter les données OpenData des Transmusicales, et d’offrir des services en lien avec ces données. Ces données regroupent un ensemble d’artistes associésà des lieux

Ce projet comporte les fonctionnalités ci-dessous :
- Une page de connexion et une page pour s'inscrire.
- L’exploration des différents artistes qui participait aux Transmusicales par une liste, par la recherche selon différents critères (nom, année de création, pays d'origine) et par l’exploration d’une carte interactive affichant précisement d'ou provienne les artistes.
- La possibilité de cliquer sur un artiste afin d'afficher des informations supplémentaires (événements dans lesquels il est apparu, dates, lieux, etc)
- Une redirection vers les albums des artistes sur spotify et deezer.
- La possibilité d'ajouter des artistes en favoris et d'afficher uniquement ces artistes.
- La possibilité de mettre des commentaires dans la page d'un artiste.
- La possibilité de noté un artiste et de visualiser ça moyenne.


