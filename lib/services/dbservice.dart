import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:transmusicales/Models/comentaire.dart';

import '../Models/artiste.dart';
import '../Models/favoris.dart';

class DbService {
  // Déclaraction et Initialisation
  final auth = FirebaseAuth.instance;
  final currentUser = FirebaseFirestore.instance
      .collection("User")
      .doc(FirebaseAuth.instance.currentUser?.uid.toString());

  Future<List<Artiste>> getArtistes(tri) async {
    return (await FirebaseFirestore.instance
            .collection("artistes_test")
            .orderBy(tri)
            .get())
        .docs
        .map((e) => Artiste(e.data()))
        .toList();
  }

  void updateRating(String nameArtiste, double rank) async {
    FirebaseFirestore.instance
        .collection("artistes_test")
        .where("artistes", isEqualTo: nameArtiste)
        .get()
        .then((value) => value.docs.first.reference
            .collection("Notes")
            .doc(currentUser.id)
            .set({"value": rank}));
  }

  Future<double> getAverageRanking(String nameArtiste) async {
    return await FirebaseFirestore.instance
        .collection("artistes_test")
        .where("artistes", isEqualTo: nameArtiste)
        .get()
        .then((value) => value.docs.first.reference
            .collection("Notes")
            .get()
            .then((notes) => calculerMoyenne(notes)));
  }

  void addFavoris(String nameArtiste) {
    currentUser
        .collection("favoris")
        .add({"artiste": nameArtiste, "value": true});
  }

  void deleteFavoris(String nameArtiste) {
    currentUser.collection("favoris").doc().get();
  }

  void addComment(String comment, String nameArtiste, String nameUser) {
    FirebaseFirestore.instance
        .collection("artistes_test")
        .where("artistes", isEqualTo: nameArtiste)
        .get()
        .then((value) =>
            value.docs.first.reference.collection("commentaire").add({
              "nom": nameUser,
              "nomArtistes": nameArtiste,
              "commentaire": comment,
              "heure": DateTime.now().microsecondsSinceEpoch,
            }));
  }

  Future<List<Commentaire>> getComment(String nameArtiste) async {
    return (await FirebaseFirestore.instance
            .collectionGroup("commentaire")
            .where("nomArtistes", isEqualTo: nameArtiste)
            .orderBy("heure")
            .get())
        .docs
        .map((e) => Commentaire(e.data()))
        .toList();
  }

  Future<List<Favoris>> getFavoris() async {
    return (await currentUser.collection("favoris").get())
        .docs
        .map((e) => Favoris(e.data()))
        .toList();
  }

  calculerMoyenne(QuerySnapshot<Map<String, dynamic>> notes) {
    int nbNotes = notes.size;
    num sommeNotes = 0;
    for (var note in notes.docs) {
      sommeNotes = sommeNotes + note.data().values.first;
    }
    //log((sommeNotes / nbNotes).toString());
    return sommeNotes / nbNotes;
  }
}
