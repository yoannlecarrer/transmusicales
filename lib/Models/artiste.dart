class Artiste {
  final String name;
  final double latitude;
  final double longitude;
  final String edition;
  final int annee;
  final String? originePays;
  late final bool favoris;

  // date
  final String? date1;
  final String? date2;
  final String? date3;
  final String? date4;

  // salle
  final String? salle1;
  final String? salle2;
  final String? salle3;
  final String? salle4;

  // Projet
  final String? projet1;
  final String? projet2;
  final String? projet3;
  final String? projet4;

  // réseaux
  final String? spotify;
  final String? deezer;

  Artiste(Map<String, dynamic> map)
      : name = map['artistes'],
        latitude = map['geo_point_2d'][0],
        longitude = map['geo_point_2d'][1],
        edition = map['edition'],
        annee = int.parse(map['annee']),
        originePays = map['origine_pays1'],
        favoris = false,
        date1 = map['1ere_date'],
        date2 = map['2eme_date'],
        date3 = map['3eme_date'],
        date4 = map['4eme_date'],
        salle1 = map['1ere_salle'],
        salle2 = map['2eme_salle'],
        salle3 = map['3eme_salle'],
        salle4 = map['4eme_salle'],
        projet1 = map['1ere_projet'],
        projet2 = map['2eme_projet'],
        projet3 = map['3eme_projet'],
        projet4 = map['4eme_projet'],
        spotify = map['spotify'],
        deezer = map['deezer'];
}

