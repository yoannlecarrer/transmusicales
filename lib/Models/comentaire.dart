class Commentaire {
  final String nom;
  final String commentaire;
  final int heure;

  Commentaire(Map<String, dynamic> map)
      : nom = map['nom'],
        commentaire = map['commentaire'],
        heure = map['heure'];
}