class Favoris {
  final String nomArtisite;
  final bool value;

  Favoris(Map<String, dynamic> map)
      : nomArtisite = map['artiste'],
      value = map['value'];
}