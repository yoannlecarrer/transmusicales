import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../constants.dart';
import '../styles/app_colors.dart';
import '../widgets/custom_button.dart';
import '../widgets/custom_formfield.dart';
import '../widgets/custom_richtext.dart';
import 'signin.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final _userNameController = TextEditingController();

  String get userName => _userNameController.text.trim();

  final _userEmailController = TextEditingController();

  String get userEmail => _userEmailController.text.trim();

  final _passwordController = TextEditingController();

  String get password => _passwordController.text.trim();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Constants.backDark,
          ),
          Positioned(
            top: MediaQuery.of(context).size.height * 0.08,
            child: Container(
              height: MediaQuery.of(context).size.height * 1.5,
              width: MediaQuery.of(context).size.width,
              decoration: const BoxDecoration(
                  color: AppColors.whiteshade,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(32),
                      topRight: Radius.circular(32))),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 200,
                    width: MediaQuery.of(context).size.width * 0.8,
                    margin: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.09),
                    child: Image.asset("assets/images/logo.png"),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  //UsernameField
                  CustomFormField(
                    headingText: "Nom d'utilisateur",
                    hintText: "Nom d'utilisateur",
                    obsecureText: false,
                    suffixIcon: const SizedBox(),
                    maxLines: 1,
                    textInputAction: TextInputAction.done,
                    textInputType: TextInputType.text,
                    controller: _userNameController,
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  //EmailField
                  CustomFormField(
                    headingText: "Email",
                    hintText: "Email",
                    obsecureText: false,
                    suffixIcon: const SizedBox(),
                    maxLines: 1,
                    textInputAction: TextInputAction.done,
                    textInputType: TextInputType.emailAddress,
                    controller: _userEmailController,
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  //PfpField
                  const SizedBox(
                    height: 16,
                  ),
                  //PasswordField
                  CustomFormField(
                    maxLines: 1,
                    textInputAction: TextInputAction.done,
                    textInputType: TextInputType.text,
                    controller: _passwordController,
                    headingText: "Mot de passe",
                    hintText: "Au moins 8 caractères",
                    obsecureText: true,
                    suffixIcon: IconButton(
                        icon: const Icon(Icons.visibility), onPressed: () {}),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  AuthButton(
                    onTap: () async {
                      try {
                        await FirebaseAuth.instance
                            .createUserWithEmailAndPassword(
                                email: userEmail, password: password)
                            .then((value) async => {
                                  await FirebaseFirestore.instance
                                      .collection('User')
                                      .doc(value.user?.uid)
                                      .set({
                                    'authorName': userName,
                                    'authorEmail': userEmail,
                                  }).then((value) => {
                                            Navigator.pushReplacement(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        const SignInPage()))
                                          })
                                });
                      } on FirebaseAuthException catch (e) {
                        if (e.code == 'weak-password') {
                          if (kDebugMode) {
                            print('Le mot de passe fourni est trop faible.');
                          }
                        } else if (e.code == 'email-already-in-use') {
                          if (kDebugMode) {
                            print('Le compte existe déjà pour cet email.');
                          }
                        }
                      } catch (e) {
                        if (kDebugMode) {
                          print(e);
                        }
                      }
                    },
                    text: 'Sign Up',
                  ),
                  CustomRichText(
                    discription: 'Vous avez déjà un compte ? ',
                    text: 'Connectez-vous ici',
                    onTap: () {
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const SignInPage()));
                    },
                  )
                ],
              ),
            ),
          ),
        ],
      )),
    );
  }
}
