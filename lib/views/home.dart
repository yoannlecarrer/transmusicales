import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:transmusicales/Models/favoris.dart';
import 'package:transmusicales/services/dbservice.dart';
import 'package:transmusicales/views/signin.dart';

import '../Models/artiste.dart';
import '../constants.dart';
import 'detail_artiste.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _MainLayoutState createState() => _MainLayoutState();
}

class _MainLayoutState extends State<Home> {
  String selectedValue = "artistes";
  final _dropdownFormKey = GlobalKey<FormState>();

  late GoogleMapController mapController;

  final LatLng _center = const LatLng(48.117266, -1.6777926);

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  List<DropdownMenuItem<String>> get dropdownItems {
    List<DropdownMenuItem<String>> menuItems = [
      DropdownMenuItem(child: Text("Nom"), value: "artistes"),
      DropdownMenuItem(child: Text("Année de création"), value: "annee"),
      DropdownMenuItem(child: Text("Pays"), value: "origine_pays1"),
    ];
    return menuItems;
  }

  List<Marker> _initMarker(List<Artiste> artists) {
    List<Marker> _markers = [];
    for (var i = 0; i < artists.length; i++) {
      _markers.add(
        Marker(
          markerId: MarkerId(artists[i].name),
          position: LatLng(artists[i].latitude, artists[i].longitude),
          infoWindow: InfoWindow(title: artists[i].name),
        ),
      );
    }
    return _markers;
  }

  Future<TabBarView> _initArtiste(trie) async {
    List<Artiste> artists = await DbService().getArtistes(trie);
    List<Favoris> favoris = await DbService().getFavoris();

    return TabBarView(
      physics: const NeverScrollableScrollPhysics(),
      children: [
        Column(children: [
          Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 10,
              horizontal: 10,
            ),
            child: Form(
                key: _dropdownFormKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    DropdownButtonFormField(
                        style: TextStyle(color: Colors.white, fontSize: 20),
                        decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Constants.backDark, width: 2),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          border: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Constants.backDark, width: 2),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          filled: true,
                          fillColor: Constants.backDark,
                        ),
                        validator: (value) => value == null
                            ? "Sélectionner sur quelle élément vous voulez trié"
                            : null,
                        dropdownColor: Constants.backDark,
                        value: selectedValue,
                        onChanged: (String? newValue) {
                          setState(() {
                            selectedValue = newValue!;
                          });
                        },
                        items: dropdownItems),
                  ],
                )),
          ),
          Expanded(
            child: GridView.builder(
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisSpacing: 10,
                crossAxisSpacing: 10,
                childAspectRatio: 1,
              ),
              itemCount: artists.length,
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: () {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (context) => DetailArtiste(
                            artiste: artists[index],
                          ),
                        ));
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 4.0,
                      horizontal: 2,
                    ),
                    child: Container(
                      color: Constants.backDark,
                      child: Text(
                        "- Nom : ${artists[index].name}\n- Année de création : ${artists[index].annee}\n- Pays d'origine : ${artists[index].originePays}",
                        style: TextStyle(color: Constants.white, fontSize: 20),
                      ),
                    ),
                  ),
                );
              },
            ),
          )
        ]),
        GoogleMap(
          onMapCreated: _onMapCreated,
          initialCameraPosition: CameraPosition(
            target: _center,
            zoom: 2.0,
          ),
          markers: Set<Marker>.of(_initMarker(artists)),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 10.0,
            horizontal: 10,
          ),
          child: ListView.builder(
            itemCount: favoris.length,
            itemBuilder: (context, index) {
              return Card(
                child: ListTile(
                  leading: FlutterLogo(size: 56.0),
                  title: Text("Nom : ${favoris[index].nomArtisite}"),
                ),
              );
            },
          ),
        ),
      ],
    );
  }

  FutureBuilder<TabBarView> test() {
    return FutureBuilder<TabBarView>(
        future: _initArtiste("artistes"),
        builder: (BuildContext context, AsyncSnapshot<TabBarView> snapshot) {
          if (snapshot.hasData) {
            return snapshot.data!;
          }
          return const Center(
            child: CircularProgressIndicator(),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            bottom: const TabBar(
              tabs: [
                Tab(text: ("Accueil")),
                Tab(text: ("Carte")),
                Tab(text: ("Favories")),
              ],
            ),
            backgroundColor: Constants.backDark,
            title: Text(
              "Transmusicales",
              style: TextStyle(color: Constants.white, fontSize: 25),
            ),
            actions: <Widget>[
              IconButton(
                icon: const Icon(Icons.logout),
                onPressed: () async {
                  await FirebaseAuth.instance.signOut().then((value) => {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const SignInPage()))
                      });
                },
              ),
            ],
          ),
          body: FutureBuilder<TabBarView>(
              future: _initArtiste(selectedValue),
              builder:
                  (BuildContext context, AsyncSnapshot<TabBarView> snapshot) {
                if (snapshot.hasData) {
                  return snapshot.data!;
                }
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }),
        ),
      ),
    );
  }
}
