import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:favorite_button/favorite_button.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:transmusicales/Models/artiste.dart';
import 'package:transmusicales/styles/app_colors.dart';
import 'package:url_launcher/url_launcher.dart';
import '../Models/comentaire.dart';
import '../Models/favoris.dart';
import '../services/dbservice.dart';
import 'package:comment_box/comment/comment.dart';
import 'home.dart';

class DetailArtiste extends StatefulWidget {
  final Artiste artiste;

  const DetailArtiste({Key? key, required this.artiste}) : super(key: key);

  @override
  _DetailArtisteState createState() => _DetailArtisteState();
}

class _DetailArtisteState extends State<DetailArtiste> {
  double _averagerating = 0.0;
  DbService db = DbService();

  final formKey = GlobalKey<FormState>();
  final _commentController = TextEditingController();

  Future<Column> _initCommentaire(name) async {
    List<Commentaire> comments = await DbService().getComment(name);
    return Column(children: [
      for (var i = 0; i < comments.length; i++)
        Text(
          'User : ${comments[i].nom} \n ${comments[i].commentaire} \n',
          style: GoogleFonts.poppins(
            color: const Color.fromRGBO(51, 51, 51, .54),
            fontSize: 18,
            height: 1.4,
          ),
        )
    ]);
  }

  Future<Positioned> _initFavoris() async {
    List<Favoris> favoris = await DbService().getFavoris();
    var bool = false;
    for (var i = 0; i < favoris.length; i++) {
      if (favoris[i].nomArtisite == widget.artiste.name &&
          favoris[i].value == true) {
        bool = true;
      }
    }
    return Positioned(
      top: 40,
      right: 20,
      child: FavoriteButton(
          isFavorite: bool,
          valueChanged: (_isFavorite) async {
            if (_isFavorite) {
              db.addFavoris(widget.artiste.name);
            } else {
              db.deleteFavoris(widget.artiste.name);
            }
          }),
    );
  }

  void test() async {
    List<Favoris> favoris = await DbService().getFavoris();
    print(favoris[0].value);
  }

  /**/

  @override
  Widget build(
    BuildContext context,
  ) {
    var mediaQuery = MediaQuery.of(context);

    majAverageRating(widget.artiste.name).then((value) => {
          setState(() {
            _averagerating = value;
          })
        });
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            height: mediaQuery.size.height / 3,
            child: Container(
              decoration: const BoxDecoration(
                color: AppColors.blackshade,
              ),
              child: Transform.translate(
                offset: const Offset(0, 0),
                child: Image.asset(
                  'assets/images/details-page-header.png',
                ),
              ),
            ),
          ),
          Positioned(
            top: 40,
            left: 10,
            child: IconButton(
              onPressed: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => const Home()));
              },
              icon: const Icon(
                Icons.arrow_back,
                color: Colors.white,
                size: 48.0,
              ),
            ),
          ),
          FutureBuilder<Positioned>(
              future: _initFavoris(),
              builder: (BuildContext context,
                  AsyncSnapshot<Positioned> snapshot) {
                if (snapshot.hasData) {
                  return snapshot.data!;
                }
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            height: mediaQuery.size.height / 1.4,
            child: Container(
              padding: const EdgeInsets.all(25),
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25),
                  topRight: Radius.circular(25),
                ),
              ),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.artiste.name,
                      style: GoogleFonts.poppins(
                        color: const Color.fromRGBO(97, 90, 90, 1),
                        fontSize: 23,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        if (widget.artiste.date4 != null) ...[
                          Expanded(
                            child: Text(
                              'Est apparu au transmusicales en ${widget.artiste.annee} pendant l\'edition "${widget.artiste.edition}. C\'est un groupe originaire de ${widget.artiste.originePays}.\n'
                              'Ils ont réaliser 4 date au transmusicales : \n'
                              '- ${widget.artiste.date1} dans la salle "${widget.artiste.salle1}"\n'
                              '- ${widget.artiste.date2} dans la salle "${widget.artiste.salle2}"\n'
                              '- ${widget.artiste.date3} dans la salle "${widget.artiste.salle3}"\n'
                              '- ${widget.artiste.date4} dans la salle "${widget.artiste.salle4}"\n',
                              style: GoogleFonts.poppins(
                                color: const Color.fromRGBO(51, 51, 51, .54),
                                fontSize: 18,
                                height: 1.4,
                              ),
                            ),
                          ),
                        ] else if (widget.artiste.date3 != null) ...[
                          Expanded(
                            child: Text(
                              'Est apparu au transmusicales en ${widget.artiste.annee} pendant l\'edition "${widget.artiste.edition}. C\'est un groupe originaire de ${widget.artiste.originePays}.\n'
                              'Ils ont réaliser 3 date au transmusicales : \n'
                              '- ${widget.artiste.date1} dans la salle "${widget.artiste.salle1}"\n'
                              '- ${widget.artiste.date2} dans la salle "${widget.artiste.salle2}"\n'
                              '- ${widget.artiste.date3} dans la salle "${widget.artiste.salle3}"\n',
                              style: GoogleFonts.poppins(
                                color: const Color.fromRGBO(51, 51, 51, .54),
                                fontSize: 18,
                                height: 1.4,
                              ),
                            ),
                          ),
                        ] else if (widget.artiste.date2 != null) ...[
                          Expanded(
                            child: Text(
                              'Est apparu au transmusicales en ${widget.artiste.annee} pendant l\'edition "${widget.artiste.edition}. C\'est un groupe originaire de ${widget.artiste.originePays}.\n'
                              'Ils ont réaliser 2 date au transmusicales : \n'
                              '- ${widget.artiste.date1} dans la salle "${widget.artiste.salle1}"\n'
                              '- ${widget.artiste.date2} dans la salle "${widget.artiste.salle2}"\n',
                              style: GoogleFonts.poppins(
                                color: const Color.fromRGBO(51, 51, 51, .54),
                                fontSize: 18,
                                height: 1.4,
                              ),
                            ),
                          ),
                        ] else ...[
                          Expanded(
                            child: Text(
                              'Est apparu au transmusicales en ${widget.artiste.annee} pendant l\'edition "${widget.artiste.edition}. C\'est un groupe originaire de ${widget.artiste.originePays}.\n'
                              'Ils ont réaliser une date au transmusicales : \n'
                              '- ${widget.artiste.date1} dans la salle "${widget.artiste.salle1}"\n',
                              style: GoogleFonts.poppins(
                                color: const Color.fromRGBO(51, 51, 51, .54),
                                fontSize: 18,
                                height: 1.4,
                              ),
                            ),
                          ),
                        ]
                      ],
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Text(
                      'Plus d\'options',
                      style: GoogleFonts.poppins(
                        color: const Color.fromRGBO(97, 90, 90, .54),
                        fontWeight: FontWeight.w700,
                        fontSize: 18,
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                      height: 100,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: [
                          AspectRatio(
                            aspectRatio: 3.5 / 1,
                            child: Container(
                              padding: const EdgeInsets.all(13),
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: (Colors.grey[300]!),
                                ),
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: RatingBar(
                                        initialRating: 0,
                                        direction: Axis.horizontal,
                                        allowHalfRating: true,
                                        itemCount: 5,
                                        ratingWidget: RatingWidget(
                                            full: const Icon(Icons.star,
                                                color: Colors.yellow),
                                            half: const Icon(
                                              Icons.star_half,
                                              color: Colors.yellow,
                                            ),
                                            empty: const Icon(
                                              Icons.star_outline,
                                              color: Colors.yellow,
                                            )),
                                        onRatingUpdate: (value) {
                                          majRating(widget.artiste.name, value);
                                        }),
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      if (_averagerating.toString() !=
                                          "NaN") ...[
                                        Text(
                                          'Note Moyenne',
                                          style: GoogleFonts.poppins(
                                            color: const Color.fromRGBO(
                                                97, 90, 90, 1),
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 2,
                                        ),
                                        Text(
                                          '$_averagerating/5',
                                          style: GoogleFonts.poppins(
                                            color: const Color.fromRGBO(
                                                97, 90, 90, .7),
                                            fontSize: 13,
                                          ),
                                        ),
                                      ] else ...[
                                        Text(
                                          'Note Moyenne',
                                          style: GoogleFonts.poppins(
                                            color: const Color.fromRGBO(
                                                97, 90, 90, 1),
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 2,
                                        ),
                                        Text(
                                          'Pas de note',
                                          style: GoogleFonts.poppins(
                                            color: const Color.fromRGBO(
                                                97, 90, 90, .7),
                                            fontSize: 13,
                                          ),
                                        ),
                                      ]
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    Column(children: [
                      if (widget.artiste.spotify != null &&
                          widget.artiste.deezer == null) ...[
                        GestureDetector(
                          onTap: () async {
                            _launchSpotify(widget.artiste);
                          },
                          child: Container(
                            height: 60,
                            padding: const EdgeInsets.symmetric(
                              horizontal: 40,
                            ),
                            decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius: BorderRadius.circular(50),
                              boxShadow: [
                                BoxShadow(
                                  color: (Colors.grey[300]!),
                                  blurRadius: 10,
                                  offset: const Offset(0, 10),
                                ),
                              ],
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.baseline,
                                  textBaseline: TextBaseline.alphabetic,
                                ),
                                Text(
                                  'Spotify',
                                  style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontSize: 25,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ] else if (widget.artiste.spotify == null &&
                          widget.artiste.deezer != null) ...[
                        GestureDetector(
                          onTap: () async {
                            _launchDeezer(widget.artiste);
                          },
                          child: Container(
                            height: 60,
                            padding: const EdgeInsets.symmetric(
                              horizontal: 40,
                            ),
                            decoration: BoxDecoration(
                              color: Colors.black54,
                              borderRadius: BorderRadius.circular(50),
                              boxShadow: [
                                BoxShadow(
                                  color: (Colors.grey[300]!),
                                  blurRadius: 10,
                                  offset: const Offset(0, 10),
                                ),
                              ],
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.baseline,
                                  textBaseline: TextBaseline.alphabetic,
                                ),
                                Text(
                                  'Deezer',
                                  style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontSize: 25,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ] else if (widget.artiste.spotify != null &&
                          widget.artiste.deezer != null) ...[
                        GestureDetector(
                          onTap: () async {
                            _launchSpotify(widget.artiste);
                          },
                          child: Container(
                            height: 60,
                            padding: const EdgeInsets.symmetric(
                              horizontal: 40,
                            ),
                            decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius: BorderRadius.circular(50),
                              boxShadow: [
                                BoxShadow(
                                  color: (Colors.grey[300]!),
                                  blurRadius: 10,
                                  offset: const Offset(0, 10),
                                ),
                              ],
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.baseline,
                                  textBaseline: TextBaseline.alphabetic,
                                ),
                                Text(
                                  'Spotify',
                                  style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontSize: 25,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        GestureDetector(
                          onTap: () async {
                            _launchDeezer(widget.artiste);
                          },
                          child: Container(
                            height: 60,
                            padding: const EdgeInsets.symmetric(
                              horizontal: 40,
                            ),
                            decoration: BoxDecoration(
                              color: Colors.black54,
                              borderRadius: BorderRadius.circular(50),
                              boxShadow: [
                                BoxShadow(
                                  color: (Colors.grey[300]!),
                                  blurRadius: 10,
                                  offset: const Offset(0, 10),
                                ),
                              ],
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.baseline,
                                  textBaseline: TextBaseline.alphabetic,
                                ),
                                Text(
                                  'Deezer',
                                  style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontSize: 25,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      ] else
                        ...[]
                    ]),
                    const SizedBox(
                      height: 50,
                    ),
                    Text(
                      'Commentaire',
                      style: GoogleFonts.poppins(
                        color: const Color.fromRGBO(97, 90, 90, .54),
                        fontWeight: FontWeight.w700,
                        fontSize: 18,
                      ),
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    FutureBuilder<Column>(
                        future: _initCommentaire(widget.artiste.name),
                        builder: (BuildContext context,
                            AsyncSnapshot<Column> snapshot) {
                          if (snapshot.hasData) {
                            return snapshot.data!;
                          }
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        }),
                    const SizedBox(
                      height: 50,
                    ),
                    Form(
                      key: formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextFormField(
                            maxLength: 100,
                            controller: _commentController,
                            decoration: const InputDecoration(
                                labelText: 'Champ de formulaire',
                                hintText: 'Entrer du texte',
                                border: OutlineInputBorder()),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return 'Veuillez saisir un texte';
                              }
                              return null;
                            },
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 16.0),
                            child: RaisedButton(
                              onPressed: () {
                                print(_commentController.text);
                                if (formKey.currentState!.validate()) {
                                  db.currentUser.get().then((value) =>
                                      db.addComment(
                                          _commentController.text,
                                          widget.artiste.name,
                                          value.get("authorName")));
                                }
                              },
                              child: Text('Envoyer'),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _launchDeezer(Artiste artiste) async {
    String url;
    url = 'deezer://www.deezer.com/fr/album/' + artiste.deezer.toString();
    if (!await launch(url)) throw 'Impossible de lancer $url';
  }

  void _launchSpotify(Artiste artiste) async {
    String? url;
    url = artiste.spotify;
    if (!await launch(url!)) throw 'Impossible de lancer $url';
  }

  void majRating(String artisteName, double rank) {
    DbService().updateRating(artisteName, rank);
  }

  Future<double> majAverageRating(String artisteName) {
    return DbService().getAverageRanking(artisteName);
  }
}
